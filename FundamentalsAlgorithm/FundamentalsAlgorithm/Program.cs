﻿using System;
using System.Collections.Generic;

namespace FundamentalsAlgorithm
{
    public static class Program
    {
        public static void Main()
        {
            //find the biggest vs Smallest
            //reverse
            //palindrome
            var word = "Madam";
            var listOfStrings = new List<string>
            {
                "madam", "easy", "Madam"
            };
            var isListPalindrome = IsPalindrome(listOfStrings);

            var isPalindrome = word.IsPalindrome();

            System.Console.WriteLine("Is " + word + " a palindrome? " + isPalindrome);
            System.Console.WriteLine("Is List a palindrome? " + isListPalindrome);


            int[] arrayInts = { 4, 6, 76, 56, 7, 3 };

            List<int> integerList = new List<int>() { 4, 6, 76, 56, 7, 3 };


            Console.WriteLine("The biggest int is : " + Biggest(arrayInts));
            Console.WriteLine("The Smallest int is : " + Smallest(arrayInts));
            var resul = Reverse(integerList);

            foreach (var item in resul)
            {
                Console.Write(item);
            }



            ////Let us try a few declarations
            //int a = 4;
            //int height = 23;
            //int weight = 40;
            //string name = "Olaolu";

            ////this
            ////if
            //if (name.CompareInvariant("olaolu"))
            //{

            //}

            //foreach (var items in name)
            //{
            //    if (items.Equals('o'))
            //    {

            //        System.Console.WriteLine("Found an " + items);
            //    }

            //    System.Console.WriteLine(items);
            //}

            //int result = height.Sum(weight);

            //System.Console.WriteLine(result);

            //System.Console.WriteLine(++height);//24

            ////24 * 41
            //System.Console.WriteLine(height++ * ++weight);

            ////25 + 41 = 66
            //System.Console.WriteLine(height + weight);

            System.Console.Read();
        }

        public static List<int> Reverse(List<int> ar)
        {
            List<int> result = new List<int>();

            for (int i = ar.Count; 0 < i; i--)
            {
                result.Add(ar[i - 1]);
            }

            return result;

        }


        public static List<string> Reverse(List<string> ar)
        {
            var result = new List<string>();

            for (int i = ar.Count; 0 < i; i--)
            {
                result.Add(ar[i - 1]);
            }

            return result;

        }

        public static string Reverse(string word)
        {
            var reversedWord = "";
            for (var i = word.Length - 1; i >= 0; i--)
            {
                reversedWord += word[i];
            }

            return reversedWord;

        }

        public static int Biggest(int[] ar)
        {
            var result = ar[0];

            foreach (var current in ar)
            {
                if (current > result)
                {
                    result = current;
                }
            }

            return result;
        }
        public static int Smallest(int[] ar)
        {
            var result = ar[0];

            foreach (var current in ar)
            {
                if (current < result)
                {
                    result = current;
                }
            }

            return result;
        }

        public static bool CompareInvariant(this string a, string b)
        {
            return string.Equals(a, b, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsPalindrome(this string word)
        {
            var reversedWord = Reverse(word);

            return word.CompareInvariant(reversedWord);
        }

        public static bool IsPalindrome(this List<string> word)
        {
            var reversedWord = Reverse(word);

            for (var i = 0; i < word.Count; i++)
            {
                if (!reversedWord[i].CompareInvariant(word[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public static int Sum(this int a, int b)
        {
            return a + b;
        }
    }
}
